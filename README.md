# frugal-dice

Frugal dice is a zero dependency Dungeon and Dragons dice bag.

## Features

* It's completely statically linked. 
* It requires nothing more than having the proper operating system and architecture.
* It can be cross compiled to any architecture the zig compiler can compile, which makes it very portable.
* Has very small memory footprint and file size, which makes it possible to be run on a very inexpensive or old computers.

## Building from source

Requirements:

* git
* zig 0.11.0

You just have to run:
```
zig build -Drelease-safe --prefix ~/.local install
```
That will build and install frugal-dice in `~/.local/bin`. Of course, you can select other place, more suitable for your needs.

## How to use
To just run it from the cloned repository without installing it:
```
zig build run
```
If you have installed it, `frugal-dice` is more than enough.
It accepts the Dungeons and Dragons notation. For an example 3d6 will roll 3 6-sided dice and will calculate the sum from them. For now, things like 1d4+1 is not recognized. Maybe I'll add that in the future.

const std = @import("std");
const writer = std.io.getStdOut().writer();
const stdin = std.io.getStdIn();

fn readLine(reader: anytype, buffer: []u8) ?[]const u8 {
    var input: ?[]u8 = reader.readUntilDelimiterOrEof(
        buffer,
        '\n',
    ) catch |err| {
        std.debug.print("Failed to read the line: {}\n", .{err});

        // Drop the rest of the stream to be clean
        reader.skipUntilDelimiterOrEof('\n') catch |err_cl| {
            std.debug.print("Unable to clean the stream: {}\n", .{err_cl});
        };

        return null;
    };
    return input;
}

const Roll = struct {
    times: u32,
    sides: u32,

    fn fromString(roll_input: []const u8) ?Roll {
        // We need pointers to the captured numbers
        var times_begin: ?usize = null;
        var times_end: ?usize = null;
        var sides_begin: ?usize = null;
        // We don't need the end of sides capture,
        // because it's expected to be the end of the string.

        for (roll_input) |character, index| {
            switch (character) {
                '0'...'9' => {
                    if (times_begin == null)
                        times_begin = index
                    else if (times_end != null and sides_begin == null)
                        sides_begin = index;
                },
                'd' => {
                    if (times_begin != null and sides_begin == null)
                        times_end = index
                    else
                        return null;
                },
                else => return null,
            }
        }

        // We exit if nothing is done.
        if (sides_begin == null) return null;

        const times_slice = roll_input[times_begin.?..times_end.?];
        const sides_slice = roll_input[sides_begin.?..];
        const roll = Roll{
            .times = std.fmt.parseInt(u32, times_slice, 10) catch unreachable,
            .sides = std.fmt.parseInt(u32, sides_slice, 10) catch unreachable,
        };

        // Some safety
        if (roll.times == 0 or roll.sides == 0) return null;
        // if (roll.sides == 0) return null;

        return roll;
    }

    fn result(self: *const Roll) !void {
        var i: u32 = 0;
        var total: u32 = 0;
        const random = std.crypto.random;
        while (i < self.times) : (i += 1) {
            const die_result = random.intRangeAtMost(u32, 1, self.sides);
            try writer.print("{d}", .{die_result});
            if (i < self.times - 1) try writer.writeAll(" + ");

            total += die_result;
        }
        if (self.times > 1)
            try writer.print(" = {d}\n", .{total})
        else
            try writer.writeAll("\n");
    }
};

pub fn main() !void {
    var buffer: [10]u8 = undefined;

    try writer.writeAll("Type \"help\" for more information.\n");
    while (true) {
        try writer.writeAll(
            \\ Roll: 
        );
        var input = readLine(stdin.reader(), &buffer) orelse continue;
        // We quit when the user types "exit" or "quit".
        if (input[0] == 'e' or input[0] == 'q') break;
        if (input[0] == 'h') {
            try writer.writeAll(
                \\
                \\ User Instructions
                \\-------------------
                \\
                \\ All the commands are entered without the quotes!
                \\ The format is the standard D&D notation: [times]d[sided_dice]
                \\ For an example type "2d6" if you want to roll two six-sided dice.
                \\
                \\ Remark: Writing just the first letter from the commands bellow is enough!
                \\ "quit" or "exit" close the program.
                \\ "help" prints this message.
                \\
                \\
            );

            continue;
        }

        if (Roll.fromString(input)) |roll| {
            try roll.result();
        } else {
            try writer.writeAll("I don't understand that. Type \"help\" for user instructions.\n");
        }
    }
}
